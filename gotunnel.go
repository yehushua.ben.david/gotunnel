package main

import (
	"bytes"
	"errors"
	"fmt"
	"log"
	"net"
	"os"
	"strings"
	"time"
)

type Sreplace struct {
	needle  []byte
	new []byte
}

var params []string
var sreplace []Sreplace

func main() {
	if len(os.Args) < 2 {
		fmt.Println("usage gotunnel port:host:port  [needle newtext]")
		return
	}
	params = strings.Split(os.Args[1], ":")
	if len(params) != 3 {
		fmt.Println("usage gotunnel port:host:port [needle newtext]")
		return
	}
	for i:=2; i<len(os.Args); i+=2 {
		sr := Sreplace{
			needle: []byte(os.Args[i]),
		}
		if i+1 < len(os.Args) {
			sr.new = []byte(os.Args[i+1])
		}
		sreplace = append(sreplace,sr)
	}
	l, err := net.Listen("tcp", ":"+params[0])
	if err != nil {
		log.Fatal(err)
	}
	for {
		c, _ := l.Accept()
		go handleClient(c)
	}

}
func handleClient(c net.Conn) {
	dest, err := net.Dial("tcp", fmt.Sprintf("%s:%s", params[1], params[2]))
	if err != nil {
		log.Println(err)
		c.Close()
		return
	}
	stoper := make(chan error)
	go tunnel(stoper, dest, c, "")
	go tunnel(stoper, c, dest, "")
}
func tunnel(stoper chan error, c1 net.Conn, c2 net.Conn, label string) {
	for {
		select {
		case reason := <-stoper:
			fmt.Println("Stop", reason)
			c1.Close()
			c2.Close()
			return
		case _ = <- time.After(5*time.Second):
			stoper<- errors.New("Time out")
		case _ = <-func() chan bool {
			rst := make(chan bool)
			go func() {
				buff := make([]byte, 65536*10)
				nb, err := c1.Read(buff)
				if err != nil {
					stoper <- err
					rst <- true
					return
				}

				wbuff := buff[:nb]
				for _,sr := range sreplace {

					wbuff = bytes.ReplaceAll(wbuff,
						sr.needle,
						sr.new)
				}
				fmt.Println(label)
				fmt.Println(string(wbuff))
				_, err = c2.Write(wbuff)
				if err != nil {
					stoper <- err
				}
				rst <- true
			}()
			return rst
		}():
			continue
		}
	}
}
